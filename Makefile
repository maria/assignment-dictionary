ASM=nasm
ASMFLAGS= -f elf64

all: lab2
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
lab2: main.o dict.o lib.o
	ld -o $@ $^

.PHONY: clean

clean:
	rm -rf *.o lab2

