%define size_buf 256
%include "words.inc"
%include "lib.inc"
extern find_word
section .data
buffer: times buff_size db 0
section .rodata
word_error: db "word not found", 10, 0
section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, size_buf
	call read_word
	mov rdi, rax
	mov rsi, myfunc
	call find_word
	test rax, rax
	jz .not_found
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi,rax
	mov rsi, 1
	call print_string
	call print_newline
	call exit
.not_found:
	mov rdi, word_error
	mov rsi, 2
	call print_string
	call exit
